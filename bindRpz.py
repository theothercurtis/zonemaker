#!/usr/local/bin/python3
import requests
import re
import sys
import fileinput
from pathlib import Path
from jinja2 import Environment, FileSystemLoader

# outputZoneDir = "/var/lib/docker/volumes/bindvol/_data/"
outputZoneDir = "./output/"
outputLocal = "named.conf.local"
outputLocalFile = outputZoneDir + outputLocal

rpzList = []
urlList = []

f = open(outputLocalFile, "wt")

namedHeader = """//
// Do any local configuration here
//

// Consider adding the 1918 zones here, if they are not used in your
// organization
//include "/etc/bind/zones.rfc1918";

zone "rpz.block" {
	type master;
	file "/etc/bind/db.rpz";
}; 
"""

f.write(namedHeader)
f.close()

zoneFiles = []
sourceDir = Path('./zonedata/')
zoneFiles.extend(sourceDir.glob("./*.rpz"))

def main():
    outputZoneName = "db.rpz"
    outputZoneFile = outputZoneDir + outputZoneName
    with open(outputZoneFile, "wt") as f:
        f.write("")
    f.close()
    #create Jinja2 environment object and refer to templates directory
    
    e = Environment(loader=FileSystemLoader('./'))
    rpzTemplate = e.get_template('rpz.j2')

    for zone in zoneFiles:
        with open(zone, "rt") as urlFile:
            urlLine = urlFile.read().splitlines()
        for line in urlLine:
            urlList.append(line)
    # print(urlList)

    for index,url in enumerate(urlList):
        # print("the url we're working on now is %s: %s" %(index,url))
        r = requests.get(url, allow_redirects=True)
        for index,item in enumerate(r.content.splitlines()):
            item = str(item, 'UTF-8')
            if re.match("^[^#].*", item):
                rpzList.append(item) 
    rpzSortedList = list(set(rpzList))
    rpzSortedList.sort()

    zoneOutput = rpzTemplate.render(o = rpzSortedList)

    with open(outputZoneFile, 'at') as f:
        for line in zoneOutput:
            f.write(line)
    f.close()

if __name__ == "__main__":
    main()

