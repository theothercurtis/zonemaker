#!/bin/python3
# -*- coding: utf-8 -*-
#
#Copyright (c) 2019 WAHED Shah Mohsin
#This code is under MIT licence, you can find the complete file here: https://github.com/shahwahed/BindZoneMaker/blob/master/LICENSE

import yaml
import json
import sys
import datetime
import getopt
import sys
import fileinput
from pathlib import Path
from jinja2 import Environment, FileSystemLoader

outputZoneDir = "/var/lib/docker/volumes/bindvol/_data/"
#outputZoneDir = "./output/"
outputLocal = "named.conf.local"
outputLocalFile = outputZoneDir + outputLocal

f = open(outputLocalFile, "wt")
namedHeader = """//
// Do any local configuration here
//

// Consider adding the 1918 zones here, if they are not used in your
// organization
//include "/etc/bind/zones.rfc1918";

"""
f.write(namedHeader)
f.close()
    
zoneFiles = []
sourceDir = Path('./zonedata/')
zoneFiles.extend(sourceDir.glob("./*.json"))

def main():
    try:
        for zone in zoneFiles:
            dnszonejson = json.loads(open(zone).read())
            outputZoneName = "db." + dnszonejson['origin'][:-1]
            outputZoneFile = outputZoneDir + outputZoneName

            #create Jinja2 environment object and refer to templates directory
            env = Environment(loader=FileSystemLoader('.'))
    
            zoneTemplate = env.get_template('zone.j2')
            namedTemplate = env.get_template('named.local.j2')
    
            zoneTemplate.globals['now'] = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
            namedTemplate.globals['zone'] = dnszonejson['origin'][:-1]
    
    #        print(template.render(dnszonejson))
            try:
                with open(outputZoneFile, 'wt') as f:
                    zoneOutput = zoneTemplate.render(dnszonejson)
                    for line in zoneOutput:
                        f.write(line)
                f.close()
            except:
                print ("zone file handle issue")
            try:
                with open(outputLocalFile, "at") as f:
                    namedOutput = namedTemplate.render(dnszonejson)
                    for line in namedOutput:
                        f.write(line)
                f.close()
            except:
                print("named.conf.local file handle issue")
    except (OSError) as ex:
        print(ex)
    except:
        print("An unexpected error occurred")
        raise

if __name__ == "__main__":
    main()
